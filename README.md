**This is a fork of DATeS**
===========================
This version of DATeS is cleaned up and simplified for the adaptive inflation and localization paper.
 
- DATeS is continuously being developed. You can get the dev-version of DATeS from: https://bitbucket.org/a_attia/dates


**DATeS: Data Assimilation Testing Suite**                       
==========================================

DATeS is aimed to be a unified testing suite for data assimilation applications where researchers can collaborate, 
so that it would be much easier to understand and compare different methodologies in different settings.
  
The core of DATeS is implemented in Python to enable for Object-Oriented capabilities. 
The main functionalities, such as the models, the data assimilation algorithms, the linear algebra solvers, 
and the time discretization routines are independent of each other, such as to offer maximum flexibility to configure data assimilation applications.
DATeS can interface easily with large third party models written in Fortran or C, and with various external solvers.


Requirements:
-------------
- Gfortran is required to wrap the FATODE time integrator for adjoint use
- Python (both 2, and 3) are supported 


Necessary Python modules:
-------------------------
- Numpy
- Scipy

Optional Python modules (will be ignored if not invoked):
---------------------------------------------------------
- Matplotlib: Optional: needed only for plotting, and some utility modules
- SkLearn: Optional: needed for HMC-Cluster sampling filters, e.g., ClHMC, MC-ClHMC, etc.

